#include "bat.h"
#include "interface.h"


int bat::attack_special(int a) {
	set_hp(get_hp() + a / 2);
	if (a>0)	use_special_phrase_2();
	return a;
}

void bat::use_special_phrase_2() {
	interface::getInterface().special_phrase(get_special_phrase_2());
}

void bat::set_special_phrase_2(std::string a) {
	special_phrase_2 = a;
}
std::string bat::get_special_phrase_2() {
	return special_phrase_2;
}