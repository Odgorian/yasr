#pragma once
#include "bird.h"

class bat : public bird {
private:
	std::string special_phrase_2;
public:
	struct batData:birdData
	{
		std::string special_phrase_2;
	};
	bat(batData &a) :bird(a) {
		set_special_phrase(a.name + " ���������� �� ����� �����.");
		set_special_phrase_2(a.name + " ���������� �� ��� �����.");
	};

	int attack_special(int a) override;
	void use_special_phrase_2();

	void set_special_phrase_2(std::string a);
	std::string get_special_phrase_2();
};