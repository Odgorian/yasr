#include "bird.h"
#include "useful.h"

int bird::defend_special(int a) {
	int b = dice(3);
	if (b == 1) {
		use_special_phrase();
		return 0;
	}
	return a;
}