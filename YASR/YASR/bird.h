#pragma once
#include "unit.h"

class bird : public unit {
public:
	struct birdData:unitData {

	};
	bird() = default;
	bird(birdData &a) :unit(a) {
		set_special_phrase(a.name + " ���������� �� ����� �����.");
	}
	int defend_special(int a) override;
};