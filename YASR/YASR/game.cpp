#include "unit.h"
#include "game.h"
#include "interface.h"
#include "hero.h"

bool fight(hero& hero, unit& monster) {
	bool wanna_fight = true;
	monster.desc();
	while (hero.get_alive() && monster.get_alive() && wanna_fight) {
		int ch = 0;
		while (ch < 1 || ch > 4)
			ch = interface::getInterface().choice_str_out(0);
		switch (ch)
		{
		case 1:
			hero.attack(monster);
			break;
		case 2:
			wanna_fight = false;
			break;
		case 3:
			monster.desc();
			break;
		case 4:
			hero.desc();
			break;
		}
		if (ch < 3) {
			if (monster.get_alive())
				monster.attack(hero);
			if (!wanna_fight && hero.get_alive())
				interface::getInterface().escape();
			if (!hero.get_alive())
				interface::getInterface().hero_death();
			if (!monster.get_alive())
				interface::getInterface().monster_death();
		}
	}
	if (wanna_fight)
		return true;
	return false;
}

void reward(hero& hero, int money) {
	hero.set_gold(hero.get_gold() + money);
	interface::getInterface().reward(money, hero.get_gold());
	hero.win();
}