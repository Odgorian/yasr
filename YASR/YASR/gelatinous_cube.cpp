#include "gelatinous_cube.h"
#include "interface.h"


int gelatinous_cube::defend_special(int damage) {
	set_def(get_def() - 2);
	interface::getInterface().special_phrase(get_special_phrase());
	return damage / 2 + damage % 2;
}
