#pragma once
#include "unit.h"

class gelatinous_cube :public unit {
	public:
		struct gelatinous_cubeData :unitData {

		};
		gelatinous_cube(gelatinous_cubeData &a) :unit(a) {
			set_special_phrase(a.name + " �������� ������� ����� ����� �����, ������� ����� ����� �����.");
		};
		int defend_special(int a) override;
};