#include "gremlin.h"
#include "interface.h"

int gremlin::attack_special(int a) {
	interface::getInterface().special_phrase(get_special_phrase());
	set_atk_mod(get_atk_mod() + 3);
	return a + 3;
}