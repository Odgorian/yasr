#include "hero.h"
#include "useful.h"

int hero::defend_special(int a) {
	if (get_hp() - a <= 0) {
		if (dice(2) == 1) {
			if (get_hp()<get_max_hp()/2)
				set_hp(get_max_hp() / 2);
			set_atk_mod(get_atk_mod() + 1);
			use_special_phrase();
			return 0;
		}
	}
	return a;
}

void hero::win() {
	win_counter++;
}

void hero::set_gold(int a) {
	gold = a;
}
int hero::get_gold() {
	return gold;
}

int hero::get_win_counter() {
	return win_counter;
}