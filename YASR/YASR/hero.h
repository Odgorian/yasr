#pragma once
#include "unit.h"

class hero: public unit{
private:
	int gold;
	int win_counter = 0;
public:
	struct heroData :public unitData {
	public:
		heroData() = default;
		int gold;
		int win_counter = 0;
	};

	explicit hero(heroData &a) : unit(a),
		gold(a.gold),
		win_counter(a.win_counter) {
		set_is_hero(true);
	};

	int defend_special(int a) override;
	void win();

	void set_gold(int a);
	int get_gold();
	int get_win_counter();
};