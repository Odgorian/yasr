#include "interface.h"
#include <iostream>
#include <conio.h>

void interface::initialising_structures() {
	arr_choice_str.resize(8);

	arr_choice_str[0] = choice_str(4, { "�� ������:", "�������","���������� �������","��������� �����","��������� ����" });
	arr_choice_str[1] = choice_str(3, { "�� ������ �� �������� �������. ��������� ��������:","�������� ������ (+10 ��, -5 �����)","�������� �������","��������� �� ������� �������" });
	arr_choice_str[2] = choice_str(6, { "�� ������ �������������:","�������� ����� (+1 ��� �����, -10 �����)","���� ����� (+1 ���� � ����, -10 �����)","������������� ����� (+1 ��������������� ����, -5 �����)","������������� (+1 ������, -10 �����)","��������� (+5 ��, -10 �����)","��������� �� ������� �������" });
	arr_choice_str[3] = choice_str(4, { "�� ���������� ��������� ����� ������� ��������, ������� ������ ���� � ���� ������. ��������� ��������:","��������� � ��������� �����, ��������� ������������ �� �����","��������� � ���������","��������� � ����������� ��������, ��������������� ����� ��� ���� ������ ��������","��������� �� ������� �������" });
	arr_choice_str[4] = choice_str(3, { "�� ������ ����� ��������� �����. ��������� ��������:","��������� � ������� �����","��������� � ����������� �����","��������� �� ������� �������" });
	arr_choice_str[5] = choice_str(3, { "�� ��������� ��������� ��������� ����������. ��������:","��������� �������� ����","���������� �������, ��������� ���������� ������ �� ���������","��������� �� ������� �������" });
	arr_choice_str[6] = choice_str(2, { "������ ����� ���� ��� ��������� ����:","����� ����","����" });
	arr_choice_str[7] = choice_str(5, { "�� ������ �� ������� �������. ��������� �������� ��������:","����� ��������� �� �����","����� ��������� ������","��������� ����","��������� ������� � ����������� �� ��� � Ҹ���� �����������","��������� ����" });
}

int interface::choice_str_out(int type) {
	std::cout << arr_choice_str[type].arr[0] << std::endl;
	for (int i = 1; i <= arr_choice_str[type].max; i++)
		std::cout << i << ". " << arr_choice_str[type].arr[i] << "." << std::endl;
	char a;
	std::cin >> a;
	a -= '0';
	if (a < 1 || a > arr_choice_str[type].max) {
		a = interface::choice_str_out(type);
	}
	system("cls");
	return a;

}

interface& interface::getInterface() {
	static interface inter;
	return inter;
}

void interface::deal_damage(int a, int b, bool hero, int c) {
	if (a)
		if (hero)
			std::cout << "�� ������� " << a << " �����. � ����� �������� " << b << "/" << c << " ��." << std::endl;
		else
			std::cout << "�� �������� " << a << " �����. � ��� �������� " << b << "/" << c << " ��." << std::endl;
	else
		if (hero)
			std::cout << "�� �� ������ ���� ���������� �����. � ����� �������� " << b << "/" << c << " ��." << std::endl;
		else
			std::cout << "���� �� ���� ���� ���������� ���. � ��� �������� " << b << "/" << c << " ��." << std::endl;

}

void interface::desc(unit &a) {
	std::cout << "���: " << a.get_name();
	std::cout << "." << std::endl << "��: " << a.get_hp() << "/" << a.get_max_hp() << "." << std::endl;
	std::cout << "����: " << a.get_atk_quan_of_dice() << "d" << a.get_atk_dice() << "+" << a.get_atk_mod() << "." << std::endl;
	std::cout << "������: " << a.get_def() << std::endl;
	std::cout << "��������: " << a.get_description() << std::endl;
}

void interface::escape() {
	std::cout << "�� ������� �������." << std::endl;
	_getch();
}

void interface::hero_death() {
	std::cout << "�� ������." << std::endl;
	_getch();
	exit(0);
}

void interface::monster_death() {
	std::cout << "�� ����� �������." << std::endl;
	_getch();
}

void interface::special_phrase(std::string sp) {
	std::cout << sp << std::endl;
}

void interface::error(std::string error_text) {
	std::cout << error_text << std::endl << "����������, �� ������� ��� ������." << std::endl;
	_getch();
}

void interface::reward(int money, int total) {
	std::cout << "� ������� �� ������ �� �������� " << money << " �����. ����� � ��� " << total << " �����." << std::endl;
}

void interface::amount_of_money(int total) {
	std::cout << "� ��� " << total << " �����." << std::endl << std::endl;
}

void interface::dragon() {
	std::cout << "�� ������� ���������� �� �� ����� ������� ���������. ������ �������� ������ �� ���." << std::endl;
}

void interface::spend_money_result(int money, int day, int type) {
	std::string str[2] = { "�������","����������" };
	std::cout << "�� ��������� �������� " << day << " ���� � " << money << " ����� �� " << str[type] << ". " << std::endl;
	_getch();
	system("cls");
}

void interface::not_enough_money() {
	std::cout << "�� �� ������ ���� ��� ���������." << std::endl;
	_getch();
}

void interface::prologue() {
	system("cls");
	std::cout << "�� ���� ������� �������� �������, ����� ����������� ���������� Ҹ����� ���������� ������" << std::endl << "� ������ ��� ����� ���������." << std::endl;
	std::cout << "��� ��� �� �� ����� ����, ������� ������������� � ������ ��� �����, ����� ���������, ���� ���������������." << std::endl;
	_getch();
}

void interface::final_lose() {
	std::cout << "���, �� ��������� ��������� �� Ҹ����� ����������. ������ ����� ��������� ��� � ��������� �����������." << std::endl;
	_getch();
}

void interface::final_win() {
	std::cout << "�� ��������� ���������� ������, ����� ������� Ҹ����� ���������� � ������ ������ ��� ����.������ ����� ��������� ���������." << std::endl;
	_getch();
}

int interface::day_choice(int money, int total, int type) {
	system("cls");
	std::string str[2] = { "��������","�������������" };
	std::cout << "������� ���� ���������� " << str[type] << "? ���� ���� ����� " << money << " �����." << std::endl << "����� � ��� " << total << " �����." << std::endl;
	int r;
	std::cin >> r;
	if (r <= 0)
		r = interface::day_choice(money, total, type);
	return r;
}
int interface::slot_choice() {
	system("cls");
	std::cout << "������� ����� �����: ";
	char ch;
	std::cin >> ch;
	ch -= '0';
	if (ch < 0 || ch > 9) {
		ch = interface::slot_choice();
	}
	system("cls");
	return ch;
}

void interface::slot_dont_exist() {
	std::cout << "���, � ���� ����� ��� ��� ����������� ����." << std::endl;
	_getch();
	exit(0);
}

void interface::game_saved(int a) {
	std::cout << "���� ��������� � ���� " << a << "." << std::endl;
	_getch();
	exit(0);
}

void interface::game_loaded(int a) {
	std::cout << "���� ������� ��������� �� ����� " << a << "." << std::endl;
	_getch();
	system("cls");
}