#pragma once

#include "unit.h"
#include<vector>

struct choice_str
{
	int max;
	std::vector<std::string > arr;

	choice_str(int new_max, std::vector<std::string> new_arr) {
		max = new_max;
		arr.resize(max + 1);
		for (int i = 0; i <= max; i++)
			arr[i] = new_arr[i];
	}
	choice_str() {
		max = 0;
	}
	choice_str& operator=(choice_str& other) {
		if (this != &other) {
			max = other.max;
			arr.resize(max + 1);
			for (int i = 0; i <= max; i++)
				arr[i] = other.arr[i];
		}
		return *this;
	}
};

class interface {
private:
	bool d = true;
	std::vector<choice_str> arr_choice_str;

	interface() {};
	interface(const interface&) = delete;
	interface& operator=(interface&) = delete;

public:
	void initialising_structures();
	static interface& getInterface();

	void deal_damage(int a, int b, bool hero, int c);
	void deal_no_damage(int b, bool hero, int c);
	void desc(unit &a);
	int fight();
	void escape();
	void hero_death();
	void monster_death();
	void special_phrase(std::string a);
	void error(std::string a);
	void reward(int a, int b);
	void amount_of_money(int a);
	void dragon();
	int spend_money_choice();
	int training_choice();
	int main_menu();
	void spend_money_result(int a, int b, int c);
	void not_enough_money();
	void prologue();
	void final_lose();
	void final_win();
	int fight_choice_difficulty();
	int fight_easy_choice();
	int fight_hard_choice();
	int day_choice(int a, int b, int c);
	int new_game_or_save();
	int slot_choice();
	void slot_dont_exist();
	void game_saved(int a);
	void game_loaded(int a);
	int choice_str_out(int a);
};

