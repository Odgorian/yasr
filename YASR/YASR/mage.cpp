#include "mage.h"
#include "useful.h"
#include "interface.h"

int mage::attack_special(int a) {
	interface::getInterface().special_phrase("��� ��������� �� ���� �������������!");
	return damage_value(get_atk_quan_of_dice(), get_atk_dice(), get_atk_mod(), 0);
}

int mage::defend_special(int a) {
	if (get_turn_counter() > 0) {
		interface::getInterface().special_phrase(get_special_phrase());
		set_turn_counter(get_turn_counter()-1);
		return 0;
	}
	else { 
		set_turn_counter(3); 
		interface::getInterface().special_phrase("��� ��������������� ��� ���� ���������!");
	}
	return a * 2;
}

void mage::set_turn_counter(int a) {
	turn_counter = a;
}
int mage::get_turn_counter() {
	return turn_counter;
}
