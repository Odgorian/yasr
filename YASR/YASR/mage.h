#pragma once
#include "unit.h"

class mage :public unit {
private:
	int turn_counter=3;
public:
	struct mageData:unitData
	{
		int turn_counter = 3;
	};
	mage(mageData &a) :unit(a),
		turn_counter(a.turn_counter) {
		set_special_phrase("�� ���������� ���� �� ���������.");
	};

	int attack_special(int a) override;
	int defend_special(int a) override;

	void set_turn_counter(int a);
	int get_turn_counter();
};