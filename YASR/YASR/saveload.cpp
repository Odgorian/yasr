#include "saveload.h"
#include "hero.h"
#include <fstream>
#include "interface.h"
#include <string.h>

saveload& saveload::getSaveload() {
	static saveload savel;
	return savel;
}

hero::heroData saveload::load(int a) {
	std::string address = "savefile";
	address += (char)(a + '0');
	address += ".txt";
	std::ifstream fin(address);
	if (fin.is_open()) {
		int hp, max_hp, def, atk_mod, atk_dice, atk_quan_of_dice, win_counter, gold;
		fin >> hp >> max_hp >> def >> atk_mod >> atk_dice >> atk_quan_of_dice >> win_counter >> gold;
		hero::heroData new_hero;
		new_hero.hp = hp;
		new_hero.max_hp = max_hp;
		new_hero.def = def;
		new_hero.atk_mod = atk_mod;
		new_hero.atk_dice = atk_dice;
		new_hero.atk_quan_of_dice = atk_quan_of_dice;
		new_hero.win_counter = win_counter;
		new_hero.gold = gold;
		new_hero.name = "�����";
		new_hero.description = "�����, ��������� �����.";
		fin.close();
		remove(address.c_str());
		interface::getInterface().game_loaded(a);
		return new_hero;
	}
	else {
		interface::getInterface().slot_dont_exist();
	}
}

void saveload::save(int a, hero & b) {
	std::string address = "savefile";
	address += (char)(a + '0');
	address += ".txt";
	std::ofstream fout(address);
	fout << b.get_hp() << " ";
	fout << b.get_max_hp() << " ";
	fout << b.get_def() << " ";
	fout << b.get_atk_mod() << " ";
	fout << b.get_atk_dice() << " ";
	fout << b.get_atk_quan_of_dice() << " ";
	fout << b.get_win_counter() << " ";
	fout << b.get_gold();
	fout.close();
	interface::getInterface().game_saved(a);
}