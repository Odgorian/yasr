#pragma once
#include "hero.h"

class saveload {
private:
	saveload() {};
	saveload(const saveload&) = delete;
	saveload& operator=(saveload&) = delete;
public:
	static saveload& getSaveload();
	hero::heroData load(int a);
	void save(int a, hero & b);
};