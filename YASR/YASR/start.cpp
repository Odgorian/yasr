#include "unit.h"
#include "game.h"
#include <clocale>
#include <time.h>
#include <stdlib.h>
#include "hero.h"
#include "bird.h"
#include "bat.h"
#include "interface.h"
#include "gelatinous_cube.h"
#include "mage.h"
#include "gremlin.h"
#include "saveload.h"


void training(hero &h) {
	int spend_money_choice = interface::getInterface().choice_str_out(1);
	switch (spend_money_choice)
	{
	case 1: {
		int p = 5;
		int d = interface::getInterface().day_choice(p, h.get_gold(), 0);
		if (h.get_gold() > p * d - 1) {
			h.set_gold(h.get_gold() - p * d);
			interface::getInterface().spend_money_result(p * d, d, 0);
			h.set_hp(h.get_hp() + 10 * d);
		}
		else
			interface::getInterface().not_enough_money();
		break;
	}
	case 2: {
		int training_menu_choice = interface::getInterface().choice_str_out(2);
		int p = 10;
		if (training_menu_choice == 3)
			p = 5;
		if (training_menu_choice != 6) {
			int d;
			d = interface::getInterface().day_choice(p, h.get_gold(), 1);
			if (h.get_gold() > p * d - 1) {
				h.set_gold(h.get_gold() - p * d);
				interface::getInterface().spend_money_result(p * d, d, 1);
				switch (training_menu_choice)
				{
				case 1: {
					h.set_atk_quan_of_dice(h.get_atk_quan_of_dice() + d);
					break;
				}
				case 2: {
					h.set_atk_dice(h.get_atk_dice() + d);
					break;
				}
				case 3: {
					h.set_atk_mod(h.get_atk_mod() + d);
					break;
				}
				case 4: {
					h.set_def(h.get_def() + d);
					break;
				}
				case 5: {
					h.set_max_hp(h.get_max_hp() + 5 * d);
					h.set_hp(h.get_hp() + 5 * d);
					break;
				}
				}
			}
			else
				interface::getInterface().not_enough_money();
		}
	}
	}
}

void fighting_easy(hero &h) {
	int fight_menu_choice = interface::getInterface().choice_str_out(4);
	switch (fight_menu_choice)
	{
	case 1: {
		bat::batData smth;
		smth.name = "������� ����";
		smth.description = "��� ������, ������������� � �������� ������ ���� �����.";
		smth.max_hp = 34;
		smth.hp = 24;
		smth.def = 3;
		smth.atk_mod = 0;
		smth.atk_dice = 3;
		smth.atk_quan_of_dice = 4;
		bat monst(smth);
		unit &m = monst;
		bool d = fight(h, m);
		if (d)
			reward(h, 20);
		break;
	}
	case 2: {
		gelatinous_cube::gelatinous_cubeData smth;
		smth.name = "����������� ���";
		smth.description = "���������� ��������� ����� � ������ � ��� �����.";
		smth.max_hp = 60;
		smth.hp = 60;
		smth.def = 4;
		smth.atk_mod = 0;
		smth.atk_dice = 5;
		smth.atk_quan_of_dice = 2;
		gelatinous_cube monst(smth);
		unit &m = monst;
		bool d = fight(h, m);
		if (d)	reward(h, 40);
		break;
	}
	}
}

void fighting_hard(hero &h) {
	int fight_menu_choice = interface::getInterface().choice_str_out(3);
	switch (fight_menu_choice)
	{
	case 1: {
		mage::mageData smth;
		smth.name = "���� ���";
		smth.description = "�� ������ ����� �����������. ��� ������ ��������� ����������.";
		smth.max_hp = 50;
		smth.hp = 50;
		smth.def = 0;
		smth.atk_mod = 0;
		smth.atk_dice = 6;
		smth.atk_quan_of_dice = 4;
		mage monst(smth);
		unit &m = monst;
		bool d = fight(h, m);
		if (d)	reward(h, 100);
		break;
	}
	case 2: {
		gremlin::gremlinData smth;
		smth.name = "�������";
		smth.description = "���������, ������� � ������ � �� ���� ���� �� ���������� ��� � ��������.";
		smth.max_hp = 80;
		smth.hp = 80;
		smth.def = 4;
		smth.atk_mod = 12;
		smth.atk_dice = 0;
		smth.atk_quan_of_dice = 0;
		gremlin monst(smth);
		unit &m = monst;
		bool d = fight(h, m);
		if (d)	reward(h, 80);
		break;
	}
	case 3: {
		interface::getInterface().dragon();
		interface::getInterface().hero_death();
		break;
	}
	}
}

void fighting(hero &h) {
	int fight_menu_choice = interface::getInterface().choice_str_out(5);
	switch (fight_menu_choice)
	{
	case 1: {
		fighting_easy(h);
		break;
	}
	case 2: {
		fighting_hard(h);
		break;
	}
	}
}

void main() {
	system("mode con cols=90 lines=30");
	srand(static_cast<unsigned int>(time(NULL)));
	setlocale(LC_ALL, "");
	interface::getInterface().initialising_structures();
	int a = interface::getInterface().choice_str_out(6);
	hero::heroData smth;
	smth.name = "�����";
	smth.description = "�����, ��������� �����.";
	if (a == 1) {
		smth.max_hp = 42;
		smth.hp = 42;
		smth.def = 2;
		smth.atk_mod = 5;
		smth.atk_dice = 6;
		smth.atk_quan_of_dice = 2;
		smth.gold = 10;
		interface::getInterface().prologue();
	}
	else {
		int b = interface::getInterface().slot_choice();
		smth = saveload::getSaveload().load(b);
	}
	hero protagonist(smth);
	hero &h = protagonist;
	bool d = true;
	while (d) {
		int main_menu_choice;
		main_menu_choice = interface::getInterface().choice_str_out(7);
		switch (main_menu_choice)
		{
		case 1: {
			fighting(h);
			break;
		}
		case 2: {
			training(h);
			break;
		}
		case 3: {
			interface::getInterface().desc(h);
			interface::getInterface().amount_of_money(protagonist.get_gold());
			break;
		}
		case 4: {
			d = false;
			break;
		}
		case 5: {
			int a = interface::getInterface().slot_choice();
			saveload::getSaveload().save(a, h);
		}
		}
	}
	if (protagonist.get_win_counter() > 4) {
		interface::getInterface().final_win();
	}
	else {
		interface::getInterface().final_lose();
	}
}