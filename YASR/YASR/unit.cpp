#include "useful.h"
#include "unit.h"
#include "interface.h"

void unit::attack(unit &enemy) {
	int atk = damage_value(get_atk_quan_of_dice(), get_atk_dice(), get_atk_mod(), enemy.get_def());
	atk = attack_special(atk);
	atk = enemy.defend_special(atk);
	enemy.set_hp(enemy.get_hp() - atk); 
	interface::getInterface().getInterface().deal_damage(atk, enemy.get_hp(), get_is_hero(), enemy.get_max_hp());
}

void unit::use_special_phrase() {
	interface::getInterface().special_phrase(get_special_phrase());
}

int unit::attack_special(int damage) {
	return damage;
}

int unit::defend_special(int damage) {
	return damage;
}

void unit::desc() {
	interface::getInterface().desc(*this);
}

void unit::set_hp(int a) {
		hp = min(a, max_hp);
		if (get_hp() <= 0) {
			set_alive(false);
		}
	}
int unit::get_hp() {
		return hp;
	}

void unit::set_max_hp(int a) {
	max_hp = a;
}
int unit::get_max_hp() {
	return max_hp;
}

void unit::set_def(int a) {
	def = a;
}
int unit::get_def() {
	return def;
}

void unit::set_atk_mod(int a) {
	atk_mod = a;
}
int unit::get_atk_mod() {
	return atk_mod;
}

void unit::set_atk_dice(int a) {
	atk_dice = a;
}
int unit::get_atk_dice() {
	return atk_dice;
}

void unit::set_atk_quan_of_dice(int a) {
	atk_quan_of_dice = a;
}
int unit::get_atk_quan_of_dice() {
	return atk_quan_of_dice;
}

void unit::set_is_hero(bool a) {
	is_hero = a;
}
bool unit::get_is_hero() {
	return is_hero;
}

void unit::set_alive(bool a) {
	alive = a;
}
bool unit::get_alive() {
	return alive;
}

void unit::set_name(std::string a) {
	name = a;
}
std::string unit::get_name() {
	return name;
}

void unit::set_description(std::string a) {
	description = a;
}
std::string unit::get_description() {
	return description;
}

void unit::set_special_phrase(std::string a) {
	special_phrase = a;
}
std::string unit::get_special_phrase() {
	return special_phrase;
}