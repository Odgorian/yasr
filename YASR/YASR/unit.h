#pragma once
#include <string>

class unit {
protected:
	int hp, max_hp;
	int def;
	int atk_mod, atk_dice, atk_quan_of_dice;

	std::string description, name;
	std::string special_phrase = "";

	bool is_hero = false;
	bool alive = true;

public:
	struct unitData {
		int hp, max_hp;
		int def;
		int atk_mod, atk_dice, atk_quan_of_dice;

		std::string description, name;
		std::string special_phrase = "";

		bool is_hero = false;
		bool alive = true;
		unitData() = default;
	};
	explicit unit(unitData &a) :
		max_hp(a.max_hp),
		hp(a.hp),
		def(a.def),
		atk_mod(a.atk_mod),
		atk_dice(a.atk_dice),
		atk_quan_of_dice(a.atk_quan_of_dice),
		name(a.name),
		special_phrase(a.special_phrase),
		description(a.description){
		is_hero = false;
		alive = true;
	};
	unit() = default;
	unit(const unit &a) = default;

	void attack(unit &enemy);
	virtual int attack_special(int damage);
	virtual int defend_special(int damage);

	void desc();
	void use_special_phrase();

	void set_hp(int a);
	int get_hp();

	void set_max_hp(int a);
	int get_max_hp();

	void set_def(int a);
	int get_def();

	void set_atk_mod(int a);
	int get_atk_mod();

	void set_atk_dice(int a);
	int get_atk_dice();

	void set_atk_quan_of_dice(int a);
	int get_atk_quan_of_dice();

	void set_is_hero(bool a);
	bool get_is_hero();

	void set_alive(bool a);
	bool get_alive();

	void set_name(std::string a);
	std::string get_name();

	void set_description(std::string a);
	std::string get_description();

	void set_special_phrase(std::string a);
	std::string get_special_phrase();
};