#include "useful.h"
#include <stdlib.h>

int min(int a, int b) {
	if (a < b)
		return a;
	return b;
}

int max(int a, int b) {
	if (a > b)
		return a;
	return b;
}

int dice(int a) {
	return (rand() % a) + 1;
}

int damage_value(int a, int b, int c, int d) {
	int res = c-d;
	for (int i = 0; i < a; i++)
		res += dice(b);
	return max(res, 0);
}